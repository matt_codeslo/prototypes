Object.prototype.showPairs = function(){
    for(var key in this){
        console.log(key + ':'+this[key]);
    }
};

var myObj = {
    propOne:'planes',
    propTwo:'trains',
    propThree:'automobiles'
};

myObj.showPairs();

//but wait there's more

myArr = ['a','b','c','d'];

myArr.showPairs();

myString = 'Hi, how are you?';

myString.showPairs();