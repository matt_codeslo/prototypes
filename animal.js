var Animal = Object.create(null);
// Object.create creates a new object, using an existing object as the new objects prototype. Passing null as a parameter means this new object doesn't have a prototype.

Animal.type = 'animal';
Animal.name = 'generic';
Animal.sound = 'grrrr';
Animal.speak = function () {
    console.log('My name is ' + this.name + ', and I am a ' + this.type + ' and I say ' + this.sound);
};
console.log(Object.getPrototypeOf(Animal)); // null

var Cat = Object.create(Animal);
Cat.type = 'cat';
Cat.sound = 'meow';
Cat.purr = function(){
    console.log('purrrrrrr...');
};

// notice that Cat does not set the name property or speak method. It inherits them, from animal.
// Since most animals don't purr, we added that as a method unique to cats

console.log(Object.getPrototypeOf(Cat)); // shows Animal prototype

var Dog = Object.create(Animal);
Dog.type = 'dog';
Dog.sound = 'woof!';

// Dogs are a type of animal, but not a type of cat. Thus, they don't inherit from cat, and can't purr.

var mrWhiskers = Object.create(Cat);
mrWhiskers.name = 'Mr.Whiskers';

// Mr. Whiskers is a cat, pure and simple. He inherits almost all his properites and all of his methods from Cat, but he has a unique name.

Animal.speak();
Cat.speak();
Dog.speak();
mrWhiskers.speak();
mrWhiskers.purr();
Dog.purr(); // error

// notice that our Animal, Cat and Dog prototypes could all run their methods. They are fully functioning objects. This is different from a classical language!
