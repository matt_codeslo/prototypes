
o = Object.create(Object.prototype, {
  // foo is a regular 'value property'
  foo: { writable: true, configurable: true, value: 'hello' },
  // bar is a getter-and-setter (accessor) property
  bar: {
    configurable: false,
    get: function() { return 10; },
    set: function(value) { console.log('Setting `o.bar` to', value); }
/* with ES5 Accessors our code can look like this
    get function() { return 10; },
    set function(value) { console.log('setting `o.bar` to', value); } */
  }
});

var p = Object.create(o);

console.log(p.foo); // logs value of 'foo'

console.log(p.bar); // triggers get function

p.bar = 3; // triggers set function, logs 'setting `o.bar` to 3.'