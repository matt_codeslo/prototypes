var Employee = Object.create(null); // null is at the top of the prototype chain

Employee.fname = 'fname';
Employee.lname = 'lname';
Employee.salary = 20000;
Employee.bio = function () {
    console.log('First Name:'+this.fname+', Last Name:'+this.lname+', salary:'+this.salary);
};

var Developer = Object.create(Employee);
Developer.salary = 80000;
Developer.writeAwesomeCode = function(){
    console.log("Writing awesome code!");
};

var Manager = Object.create(Employee);
Manager.salary = 100000;
Manager.goToMeeting = function(){
    console.log("I'm in a super-important meeting right now.");
};

var Sales = Object.create(Employee);

Sales.salary = 50000;
Sales.earnCommission = function(){
    this.salary *= 2;
    console.log(this.salary);
};

var lisa = Object.create(Developer);
lisa.fname = "Lisa";
lisa.lname = "Brown";
lisa.bio();
lisa.writeAwesomeCode();

var karen = Object.create(Manager);
karen.fname = "Karen";
karen.lname = "Jones";
karen.bio();
karen.goToMeeting();


var bob = Object.create(Sales);
bob.fname = "Bob";
bob.lname = "Smith";
bob.bio();
bob.earnCommission();


