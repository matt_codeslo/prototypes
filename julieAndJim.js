var kid = Object.create(null);
kid.name = 'name';
kid.speak = function(){
    console.log(this.name);
};

//julie is shy, and will only say her name after a 2 second delay

var julie = Object.create(kid);
julie.name = 'Julie';
julie.speak = function(){
    setTimeout(function(){
        console.log(julie.name);
    },2000);
};

// jim is very enthusiastic and always shouts.

var jim = Object.create(kid);
jim.name = "Jim";
jim.speak = function(){
    console.log(this.name.toUpperCase()+'!!!');
};

// both objects use the speak method, but that speak method does different things. This is a very crude example of polymorphism. A better example would be a method that could deal with data that differed from derived type to derived type.

julie.speak();
jim.speak();
